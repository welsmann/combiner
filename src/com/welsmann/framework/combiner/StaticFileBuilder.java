package com.welsmann.framework.combiner;

import java.io.File;
import java.util.Vector;

/**
 * 静态资源文件路径构造工具类
 * @author Welsmann
 *
 */
public class StaticFileBuilder {

	public StaticFileBuilder() {
		
	}
	
	public Vector<String> build(String type, String project, String files, String prefix) {
		Vector<String> staticFiles = new Vector<String>();
		if (StringUtil.validateString(type, project, files)) {
			 String[] strFileArray = files.split(",");
			 for (String f : strFileArray) {
				 staticFiles.add(type + project + File.separator + buildFilePath(f) + "." + prefix);
			 }
		}
		return staticFiles;
	}
	
	private String buildFilePath(String f) {
		String[] fs = f.split(":");
		String path = "";
		for (String str : fs) {
			path += File.separator + str;
		}
		return path.substring(1);
	}
	
}
