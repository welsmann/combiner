package com.welsmann.framework.combiner;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Vector;

public class StreamEnumerator implements Enumeration<FileInputStream>{

	private Enumeration<String> files;
	
	public StreamEnumerator(Vector<String> files)
	{
		this.files = files.elements();
	}
	
	public boolean hasMoreElements() {
		return files.hasMoreElements();
	}

	public FileInputStream nextElement() {
		String path = files.nextElement().toString();
		
		try
		{
			FileInputStream input = new FileInputStream(path);
			return input;
		}
		catch (Exception e)
		{
			return null;
		}
	}

}
