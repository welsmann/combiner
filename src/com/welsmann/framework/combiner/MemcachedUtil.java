package com.welsmann.framework.combiner;

import org.apache.log4j.BasicConfigurator;

import com.welsmann.framework.cache.memcached.MemcachedClient;
import com.welsmann.framework.cache.memcached.SockIOPool;

/**
 * Memcached缓存工具类
 * @author Welsmann
 *
 */
public class MemcachedUtil {
	
	protected static MemcachedClient client = new MemcachedClient();
	
	protected static MemcachedUtil memcachedUtil = new MemcachedUtil();
	
	static{
		BasicConfigurator.configure();
		String[] servers = new String[]{"127.0.0.1:11211"};
		Integer[] weights = {1};
		SockIOPool pool = SockIOPool.getInstance();
		pool.setServers(servers);
		pool.setWeights(weights);
		pool.setInitConn(5);
		pool.setMinConn(5);
		pool.setMaxConn(500);
		pool.setMaxIdle(1000 * 60 * 60 * 4);
		pool.setMaintSleep(30);
		pool.setNagle(false);
		pool.setSocketTO(3000);
		pool.setSocketConnectTO(0);
		pool.initialize();
		client.setCompressEnable(true);
		client.setCompressThreshold(32 * 1024);
		com.welsmann.framework.cache.memcached.Logger.getLogger(MemcachedClient.class.getName())
			.setLevel(com.welsmann.framework.cache.memcached.Logger.LEVEL_ERROR);
	}
	
	protected MemcachedUtil(){
		
	}
	
	public static MemcachedUtil getInstance() {
		return memcachedUtil;
	}
	
	public String put(String key, String value) {
		return client != null && client.set(key, value) ? value : null;
	}
	
	public String get(String key) {
		return client != null && client.keyExists(key) ? (String)client.get(key) : null;
	}
	
}
