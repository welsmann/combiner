package com.welsmann.framework.combiner;

public final class StringUtil {
	
	private static String _slash = "";

	protected static boolean validateString(String... strs) {
		boolean blnRtn = true;
		if (strs == null || strs.length == 0) {
			blnRtn = false;
		} else {
			for (String str : strs) {
				if (str == null || str.trim().length() == 0) {
					blnRtn = false;
					break;
				}
			}
		}
		return blnRtn;
	}
}
